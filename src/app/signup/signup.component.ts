import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SignUp } from '../shared/signup-model';
import { AuthService } from '../login/shared/auth.service';
import { EqualValidator } from '../shared/equal-validator.directive';

@Component({
  selector: 'app-signup',
   providers: [AuthService],
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  constructor(
    private authService: AuthService,
    private router: Router 
  ) {

    
   }

  register = new SignUp('', '','','','','');

submitForm(form: any): void{
    console.log('Form Data: ');
    console.log(form);

    if (form.password != '' && form.confirmPassword != '' && form.password == form.confirmPassword) {

      if (this.authService.register(this.register)) {
        this.router.navigate(['dashboard']); 
      }
      
    }
    else
    alert("password mismatch");
  }

  ngOnInit() {
  }

}