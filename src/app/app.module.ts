import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Http } from '@angular/http';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { RouterModule } from '@angular/router';
import { DashboardModule } from './dashboard/dashboard.module';
import { AppRoutes } from './app.routing'
import { TranslateModule } from "ng2-translate";
import { TranslateLoader } from "ng2-translate";
import { TranslateStaticLoader } from "ng2-translate";
import { AuthService } from './login/shared/auth.service';




@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SignupComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    DashboardModule,
    AppRoutes,
    TranslateModule.forRoot({
      provide: TranslateLoader,
      useFactory: (createTranslateLoader),
      deps: [Http]
    })

  ],
  providers: [AuthService],
  bootstrap: [AppComponent]
})

export class AppModule { }

export function createTranslateLoader(http: Http) {
  return new TranslateStaticLoader(http, './assets/lang', '.json');
}