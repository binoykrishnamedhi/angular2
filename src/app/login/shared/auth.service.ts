import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { SignUp } from '../../shared/signup-model';
import { User } from '../../shared/user-model';
import { Observable } from 'rxjs/Observable';
import { CanActivate, Router } from '@angular/router'
import 'rxjs';

@Injectable()
export class AuthService implements CanActivate {
    JWT_KEY: string = 'token';
    JWT: string = '';
    private usersUrl = 'app/login/shared/users.json';
    islogin: boolean = false;

    constructor(private http: Http, private router: Router) { }

    canActivate(): boolean {

        if (Boolean(this.JWT)) {
            console.log("success");

            return true;
        }
        else {
            console.log("fail");
            this.router.navigate(['/login']);
        }

    }

    setJWT(value: string) {
        this.JWT = value;
    }

    login(user: User): boolean {

        if (user.userName == 'admin' && user.password == 'Infy123') {
            console.log('Hurray!! Let\'s move to the next page');
            this.islogin = true;
            return true;
        } else {
            console.log('Check Login service file for the password!!');
            this.islogin = false;
            return false;
        }
    }

    getUsers(): Observable<User[]> {
        return this.http.get(this.usersUrl)
            .map(this.extractData)
            .catch(this.handleError);
    }

    private extractData(res: Response) {
        let body = res.json();
        return body || {};
    }

    private handleError(error: Response | any) {
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    }

    isRegister: boolean = false;
    register(signup: SignUp): boolean {
        if (signup != null) {

            this.isRegister = true;
            return true;
        }
        else {
            this.isRegister = false;
            return false;
        }

    }

}