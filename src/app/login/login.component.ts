import { Component, OnInit } from '@angular/core';
import { AuthService } from './shared/auth.service';
import { User } from '../shared/user-model';
import { PasswordResetUser } from '../shared/PasswordResetUser';
import { FormsModule } from '@angular/forms';

import { Router } from '@angular/router'
import 'rxjs';
import 'rxjs/add/operator/delay';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  mode = 'Observable';
  users: User[];
  errorMessage: string;
  title = 'UT Portal';

  constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  model = new User('', '');
private isValidUsername : boolean=false;
  newModel = new PasswordResetUser('', '', '');
 
  ResetPassword(form: any): void{
    console.log(form);
   if (form.Newpassword != '' && form.ConfirmPassword != '' && form.Newpassword == form.ConfirmPassword) {
    console.log('Password reset successful!!');
    document.getElementById("close").click();
    document.getElementById("closeChange").click();
    (<any>$('#myModalWithTimeoutForSuccess')).modal();
    this.router.navigate(['login']); 
    //alert("Password Reset Successful.");  
    } 
    else
    (<any>$('#myModalWithTimeout')).modal();
  }

  CheckUser(){
    console.log("clicked checkuser");
    this.authService.getUsers()
      .subscribe(
            users =>  this.forgotPassword(users, this),
            error =>  this.errorMessage = <any>error
      );
  }


  onSubmit() {
    this.authService.getUsers()
      .subscribe(
      users => this.validate(users, this),
      error => this.errorMessage = <any>error
      );
  }

   forgotPassword(users : User[], t : LoginComponent) {
    users.forEach(function(user) {
          if (user.userName == t.model.userName) {
              console.log('Hurray!! You can change your password.');
              t.isValidUsername=true;
              return true;
          }
      });
  }

  private validate(users: User[], loginComponent: LoginComponent) {
    users.forEach(function (user) {
      if (user.userName == loginComponent.model.userName && user.password == loginComponent.model.password) {
        console.log('Hurray!! Let\'s move to the next page');
        loginComponent.authService.setJWT('somemd5()token');

        loginComponent.router.navigate(['dashboard']);
      } else {
         (<any>$('#myModalWithTimeout')).modal();
        console.log('Check Login service file for the password!!');
      }
    });
  }
}