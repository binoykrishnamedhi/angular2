import { Component } from '@angular/core';
import { Router } from '@angular/Router'
import {TranslateService} from 'ng2-translate';
import './rxjs-operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  constructor(private translate: TranslateService) {
        translate.addLangs(["en", "hi"]);
        translate.setDefaultLang('en');

        let browserLang = translate.getBrowserLang();
        translate.use(browserLang.match(/en|hi/) ? browserLang : 'en');
    }
 }
