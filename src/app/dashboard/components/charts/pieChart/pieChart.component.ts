import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pieChart',
  templateUrl: './pieChart.component.html',
  styleUrls: ['./pieChart.component.css']
})
export class PieChartComponent {

  public pieChartLabels:string[] = ['Download Sales', 'In-Store Sales', 'Mail Sales'];
   public pieChartData:number[] = [300, 500, 100];
   public pieChartType:string = 'pie';

   // events
   public chartClickedOn(e:any):void {
     console.log(e);
   }

   public chartHoveredOn(e:any):void {
    console.log(e);
   }

  

}