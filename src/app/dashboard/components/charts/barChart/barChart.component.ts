import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-barChart',
  templateUrl: './barChart.component.html',
  styleUrls: ['./barChart.component.css']
})
export class BarChartComponent {

  public barChartOptions:any = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  public barChartLabels:string[] = ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
  public barChartType:string = 'bar';
  public barChartLegend:boolean = true;

  public barChartData:any[] = [
    {data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A'},
    {data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B'}
  ];

  // events
  

  // public randomizeBar():void {
  //   // Only Change 3 values
  //   let data = [
  //     Math.round(Math.random() * 100),
  //     59,
  //     80,
  //     (Math.random() * 100),
  //     56,
  //     (Math.random() * 100),
  //     40];
  //   let clone = JSON.parse(JSON.stringify(this.barChartData));
  //   clone[0].data = data;
  //   this.barChartData = clone;
  //   /**
  //    * (My guess), for Angular to recognize the change in the dataset
  //    * it has to change the dataset variable directly,
  //    * so one way around it, is to clone the data, change it and then
  //    * assign it;
  //    */
  
  
  // }
  public randomizeBar():void {
    let _barChartData:Array<any> = new Array(this.barChartData.length);
    for (let i = 0; i < this.barChartData.length; i++) {
      _barChartData[i] = {data: new Array(this.barChartData[i].data.length), label: this.barChartData[i].label};
      for (let j = 0; j < this.barChartData[i].data.length; j++) {
        _barChartData[i].data[j] = Math.floor((Math.random() * 100) + 1);
      }
    }
    this.barChartData = _barChartData;
  }
  public chartClickedOn(e:any):void {
    console.log(e);
  }

  public chartHoveredOn(e:any):void {
    console.log(e);
  }


}