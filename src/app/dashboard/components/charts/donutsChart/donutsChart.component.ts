import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-donutsChart',
  templateUrl: './donutsChart.component.html',
  styleUrls: ['./donutsChart.component.css']
})
export class DonutsChartComponent  {

  public doughnutChartLabels:string[] = ['Download Sales', 'In-Store Sales', 'Mail-Order Sales'];
  public doughnutChartData:number[] = [350, 450, 100];
  public doughnutChartType:string = 'doughnut';

  // events
  public chartClickedOn(e:any):void {
    console.log(e);
  }

  public chartHoveredOn(e:any):void {
    console.log(e);
  }

}