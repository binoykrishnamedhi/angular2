import { Component} from '@angular/core';
import {SebmGoogleMap} from 'angular2-google-maps/core';
import { marker } from '../maps/marker';
@Component({
  selector: 'app-maps',
  templateUrl: './maps.component.html',
  styleUrls: ['./maps.component.css'],
})
export class MapsComponent
 {
 title = 'Google Maps UT-Portal';
constructor() { }
 markers: marker[] = [
                               {
                                  lat: 30.7333,
                                  lng: 76.7794,
                                  // label: 'Chandigarh',
                                  description: 'Chandigarh',
                                },
                                 {
                                  lat: 30.6942,
                                  lng: 76.8606,
                                  // label: 'Panchkula',
                                  description: 'Panchkula',
                                 },
                                 {
                                  lat: 30.9,
                                  lng: 75.8,
                                  // label: 'Ludhiana',
                                  description: 'Ludhiana',
                                  },
                                 {
                                  lat: 30.9013,
                                  lng: 76.9649,
                                  // label: 'Kasauli',
                                  description: 'Kasauli',
                                 
                                },
                                {
                                  lat: 30.3,
                                  lng: 76.4,
                                  // label: 'Patiala',
                                  description: 'Patiala',
                                  
                                }
  ]
  
  // google maps zoom level
  zoom: number = 8;
  // initial center position for the map
    lat: number = 30.7333;
    lng:number = 76.7794
  
 clickedMarker(label: string, index: number) {
 console.log(`clicked the marker: ${label || index}`)
  }
  
 
  markerDragEnd(m: marker, $event: MouseEvent) {
  console.log('dragEnd', m, $event);
  }
}
