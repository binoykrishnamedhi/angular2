import { Component} from '@angular/core';
import {SebmGoogleMap} from 'angular2-google-maps/core';


const Transactions= 
[
    {Date :'01-Nov-2016',cashless:'1234',cash:'123'},
    {Date :'04-Nov-2016',cashless:'1234',cash:'123'},
    {Date :'08-Nov-2016',cashless:'1234',cash:'123'}
];


@Component({
    selector:'transactions',
        template: `
    <table>
        <thead>
            <th>Date</th>
            <th>Cashless</th>
            <th>Cash</th>
         </thead>
        <tbody>
        <tr *ngFor="let transaction of transactions;let i = index" >
            <td>{{transaction.Date}}</td>
            <td>{{transaction.cashless}}</td>
            <td>{{transaction.cash}}</td>
        </tr>
        </tbody>
    </table>
`,
styleUrls: ['./transactions.component.css']
})

export class TransactionsComponent{

 
   transactions=Transactions;
}
