import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core'
import { ChartsComponent } from './components/charts/charts.component'
import { DashboardComponent } from './dashboard.component'
import { MapsComponent } from './components/maps/maps.component';
import { HomeComponent } from './pages/home/home.component';
import { SmartTableComponent } from './components/smartTable/smartTable.component'
import { AuthService } from '../login/shared/auth.service';


const routes: Routes = [
  {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [AuthService],
    children: [
      {
        path: '',
        component: HomeComponent
      },
      {
        path: 'home',
        component: HomeComponent
      },
      {
        path: 'charts',
        component: ChartsComponent
      },
      {
        path: 'maps',
        component: MapsComponent
      },
      {
        path: 'table',
        component: SmartTableComponent
      }
    ]
  },


];

export const DashboardRoutes: ModuleWithProviders = RouterModule.forChild(routes);
