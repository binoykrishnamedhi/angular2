import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardRoutes } from './dashboard.routing'
import { DashboardComponent } from './dashboard.component';
import { MapsComponent } from './components/maps/maps.component';
import { ChartsComponent } from './components/charts/charts.component';
import { AgmCoreModule } from 'angular2-google-maps/core';
import { HomeComponent } from './pages/home/home.component';
import { SmartTableComponent } from './components/smartTable/smartTable.component';
import { Ng2SmartTableModule } from './components/ng2-smart-table/ng2-smart-table.module';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { TransactionsComponent } from './components/maps/transactions.component';
import { BarChartComponent } from './components/charts/barChart/barChart.component';
import { LineChartComponent } from './components/charts/lineChart/lineChart.component';
import { DonutsChartComponent } from './components/charts/donutsChart/donutsChart.component';
import { PieChartComponent } from './components/charts/pieChart/pieChart.component';






@NgModule({
  imports: [
    CommonModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAkEb4wd5Tto8BSf5UOk2xfeJRXJpjIsWA'
    }),
    DashboardRoutes,
    Ng2SmartTableModule,
    ChartsModule
  ],
  declarations: [DashboardComponent,
    MapsComponent,
    ChartsComponent
    ,
    HomeComponent,
    SmartTableComponent,
    TransactionsComponent,
    BarChartComponent, 
    LineChartComponent, 
    DonutsChartComponent, 
    PieChartComponent
  ]
})
export class DashboardModule { }