export class PasswordResetUser {

  constructor(
    public userName: string,
    public password: string,
    public confirmPassword: string
  ) {  }

}