import { UtPortalPage } from './app.po';

describe('ut-portal App', function() {
  let page: UtPortalPage;

  beforeEach(() => {
    page = new UtPortalPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
